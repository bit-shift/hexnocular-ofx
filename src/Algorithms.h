#pragma once

#include <math.h>
#include <random>

namespace algorithms
{
    template<typename T>
    inline T convert_value_range(const T value, const T current_lower, const T current_upper,
                          const T target_lower, const T target_upper)
    {
        auto slope = (target_upper - target_lower) / (current_upper - current_lower);
        auto output = target_lower + slope * (value - current_lower);

        return output;
    }

    template<typename T>
    inline T radians(const T degrees)
    {
        return (degrees * M_PI) / 180;
    }

    template<typename T> 
    T generate_number(const T lower, const T upper)
    {
        std::default_random_engine generator;
        std::uniform_int_distribution<T> distribution(lower, upper);
        return distribution(generator);
    }
}

