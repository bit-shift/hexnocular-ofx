#pragma once

#include "ofMain.h"

class Settings {

public:

    static Settings* instance();

    void set_fullscreen(bool f);
    bool get_fullscreen();

private:

    Settings();

    static Settings* s_instance;

    bool _fullscreen;
};
