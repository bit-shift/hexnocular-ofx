#include <ofApp.h>

void ofApp::setup()
{
	ofBackground(0);

	// Enable or disable audio for video sources globally
	// Set this to false to save resources on the Raspberry Pi
	ofx::piMapper::VideoSource::enableAudio = false;

    lissajousSource = std::make_unique<LissajousSource>(LissajousSource{800, 600});
    neonSource = std::make_unique<NeonSource>(NeonSource{800, 600});

    piMapper.registerFboSource(*lissajousSource);
    piMapper.registerFboSource(*neonSource);
    piMapper.setup();
	
    ofSetFullscreen(Settings::instance()->get_fullscreen());
}

void ofApp::update()
{
	piMapper.update();

    // if (lissajousSource)
    //     lissajousSource->set_frame_num(ofGetFrameNum());
}

void ofApp::draw()
{
	piMapper.draw();
}
