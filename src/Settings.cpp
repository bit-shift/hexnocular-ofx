#include "Settings.h"

Settings* Settings::s_instance = nullptr;

Settings* Settings::instance()
{
    if (!s_instance)
        s_instance = new Settings();

    return s_instance;
}

Settings::Settings()
{
    _fullscreen = false;
}

void Settings::set_fullscreen(bool f)
{
	_fullscreen = f;
}

bool Settings::get_fullscreen()
{
	return _fullscreen;
}
