#pragma once

#include <ofMain.h>
#include <FboSource.h>


class NeonSource : public ofx::piMapper::FboSource
{
public:
    NeonSource(const int width, const int height);

    void update();
};
