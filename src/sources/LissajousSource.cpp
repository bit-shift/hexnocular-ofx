#include "LissajousSource.h"

#include <Algorithms.h>

LissajousSource::LissajousSource(const int width, const int height) : 
    FboSource(width, height)
{   
    name = "Lissajous";

//    _freq_x = 21, _freq_y = 1, _mod_freq_x = 7, _mod_freq_y = 1;
//    _freq_x = 21, _freq_y = 1, _mod_freq_x = 21, _mod_freq_y = 1;     // (the tube)
//    _freq_x = 25, _freq_y = 1, _mod_freq_x = 25, _mod_freq_y = 1;     // (spinning mirrors)
     _freq_x = 5, _freq_y = 1, _mod_freq_x = 3, _mod_freq_y = 2;

    update_points();
}

void LissajousSource::update()
{
    update_points();        

    fbo_.begin();

    // move this to draw function again
    ofClear(0);
    ofBackground(0);
    ofSetColor(255, 0, 186, 128);
    ofEnableAlphaBlending();

    ofTranslate(width_ / 2, width_ / 2, 0);

    for (size_t i1 = 0; i1 < _count; i1++)
    {
        for (size_t i2 = 0; i2 < i1; i2++)
        {
            Point p1 = _points[i1];
            Point p2 = _points[i2];

            auto p1_x = get<0>(p1);
            auto p1_y = get<1>(p1);
            auto p2_x = get<0>(p2);
            auto p2_y = get<1>(p2);

            float distance = sqrt(pow(p2_x - p1_x, 2) + pow(p2_y - p1_y, 2));
            float a = pow(1 / (distance / _conn_radius + 1), 6);


            if (distance <= _conn_radius)
                ofDrawLine(p1_x, p1_y, p2_x, p2_y);
        }
    }
    fbo_.end();
}

void LissajousSource::update_points()
{
    auto update = false;

    if (_frame_num % 7 == 0)
    {
        _phi += 1.0;
        update = true;
    }

    if (update)
    {
        _points.clear();

        for (size_t i = 0; i <= _count; i++)
        {
            float angle = algorithms::convert_value_range<float>(i, 0, _count, 0, M_PI * 2);
            float x = (sin(angle * _freq_x + algorithms::radians<float>(_phi)) - 0.5) * cos(angle * _mod_freq_x);
            float y = (sin(angle * _freq_y) - 0.5) * cos(angle * _mod_freq_y);

            x = x * (width_ / 2-30) + 10;
            y = y * (height_ / 2-30) + 10;

            _points.push_back(std::make_tuple(x, y));
        }
    }
    
}

void LissajousSource::set_frame_num(const int frame_num)
{
    _frame_num = frame_num;
}
