#pragma once

#include "ofMain.h"
#include "FboSource.h"

#include <vector>
#include <Types.h>

class LissajousSource : public ofx::piMapper::FboSource {

public:
    LissajousSource(const int width, const int height);
    
    void update();

    void set_frame_num(const int frame_num);

private:

    void update_points();

    std::vector<Point> _points;

    size_t _count       = 300;

    float _conn_radius  = 100;
    float _line_alpha   = 50;

    float _freq_x       = 1.f;
    float _freq_y       = 1.f;
    float _mod_freq_x   = 1.f;
    float _mod_freq_y   = 1.f;
    float _phi          = 0.f;

    int _frame_num      = 0;

};
